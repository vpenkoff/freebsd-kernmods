#include <sys/param.h>
#include <sys/systm.h>
#include <sys/kernel.h>
#include <sys/mbuf.h>
#include <sys/module.h>
#include <machine/bus.h>
#include <sys/rman.h>
#include <sys/socket.h>
#include <sys/sockio.h>
#include <sys/sysctl.h>

#include <net/if.h>
#include <net/if_clone.h>
#include <net/if_types.h>
#include <net/netisr.h>
#include <net/route.h>
#include <net/bpf.h>
#include <net/vnet.h>

#ifdef INET
#include <netinet/in.h>
#include <netinet/in_var.h>
#endif


#include <security/mac/mac_framework.h>

#define SMMTU	(1024 + 512)

#define SM_CSUM_FEATURES	(CSUM_IP | CSUM_TCP | CSUM_SCTP)
#define SM_CSUM_SET		(CSUM_DATA_VALID | CSUM_PSEUDO_HDR | \
					CSUM_IP_CHECKED | \
					CSUM_IP_VALID	| \
					CSUM_SCTP_VALID)

int		smioctl(struct ifnet *, u_long, caddr_t);
static void	smrtrequest(int, struct rtentry *, struct rt_addrinfo *);
int		smoutput(struct ifnet *, struct mbuf *, struct sockaddr *,
		    struct route *ro);
static int 	sm_clone_create(struct if_clone *, int, caddr_t);
static void	sm_clone_destroy(struct ifnet *);

IFC_SIMPLE_DECLARE(sm, 1);

static void
sm_clone_destroy(struct ifnet *ifp)
{
	bpfdetach(ifp);
	if_detach(ifp);
	if_free(ifp);
}

static int
sm_clone_create(struct if_clone *ifc, int unit, caddr_t params)
{
	struct ifnet *ifp;

	ifp = if_alloc(IFT_LOOP);
	if (ifp == NULL)
		return (ENOSPC);

	if_initname(ifp, "sm", unit);
	ifp->if_mtu = SMMTU;
	ifp->if_flags = IFF_LOOPBACK | IFF_MULTICAST;
	ifp->if_ioctl = smioctl;
	ifp->if_output = smoutput;
	ifp->if_snd.ifq_maxlen = ifqmaxlen;
	ifp->if_capabilities = ifp->if_capenable = IFCAP_HWCSUM;
	ifp->if_hwassist = SM_CSUM_FEATURES;
	if_attach(ifp);
	bpfattach(ifp, DLT_NULL, sizeof(u_int32_t));

	return (0);
}

static int
loop_modevent(module_t mod, int type, void *data)
{
	switch(type) {
		case MOD_LOAD:
			if_clone_attach(&sm_cloner);
			printf("Module sm loaded!\n");
			break;
			case MOD_UNLOAD:
			printf("Can't unload, kernel oops\n");
			return (EINVAL);
			break;
		default:
			return (EOPNOTSUPP);
	}
	return (0);
}

static moduledata_t sm_mod = {
	"if_sm",
	loop_modevent,
	0
};

DECLARE_MODULE(if_sm, sm_mod, SI_SUB_PROTO_IFATTACHDOMAIN, SI_ORDER_ANY);

int 
smoutput(struct ifnet *ifp, struct mbuf *m, struct sockaddr *dst,
    struct route *ro)
{
	u_int32_t af;
	struct rtentry *rt = NULL;

#ifdef MAC
	int error;
#endif

	M_ASSERTPKTHDR(m);

	if (ro != NULL)
		rt = ro->ro_rt;

#ifdef MAC
	error = mac_ifnet_check_transmit(ifp, m);
	if (error) {
		m_freem(m);
		return (error);
	}

#endif
	if (rt && rt->rt_flags & (RTF_REJECT | RTF_BLACKHOLE)) { 
		m_freem(m);
		return (rt->rt_flags & RTF_BLACKHOLE ? 0 : 
			rt->rt_flags & RTF_HOST ? EHOSTUNREACH : ENETUNREACH);
	}

	ifp->if_opackets++;
	ifp->if_obytes += m->m_pkthdr.len;

	if (dst->sa_family == AF_UNSPEC) {
		bcopy(dst->sa_data, &af, sizeof(af));
		dst->sa_family = af;
	}

	if (dst->sa_family == AF_INET) {
		if (ifp->if_capenable & IFCAP_RXCSUM) {
			m->m_pkthdr.csum_data = 0xffff;
			m->m_pkthdr.csum_flags = SM_CSUM_SET;
		}
		m->m_pkthdr.csum_flags &= ~SM_CSUM_FEATURES;
	}
	else {
		printf("smoutput: af=%d unexpected\n", dst->sa_family);
		m_freem(m);
		return (EAFNOSUPPORT);
	}

	return (if_simloop(ifp, m, dst->sa_family, 0));
}

int
if_simloop(struct ifnet *ifp, struct mbuf *m, int af, int hlen)
{
	int isr;

	M_ASSERTPKTHDR(m);
	m_tag_delete_nonpersistent(m);
	m->m_pkthdr.rcvif = ifp;

#ifdef MAC
	mac_ifnet_create_mbuf(ifp, m);
#endif
	if (hlen  > 0) {
		if (bpf_peers_present(ifp->if_bpf))
			bpf_mtap(ifp->if_bpf, m);
	}

	if (hlen > 0) {
		m_adj(m, hlen);
#ifndef __NO_STRICT_ALIGNMENT

		if (mtod(m, vm_offset_t) & 3) {
			KASSERT(hlen >= 3, ("if_simloop: hlen too small"));
			bcopy(m->m_data,
			    (char *)(mtod(m, vm_offset_t)
			        - (mtod(m, vm_offset_t) & 3)),
			    m->m_len);
			m->m_data -= (mtod(m, vm_offset_t) & 3);
		}
#endif
	}

	switch (af) {
	case AF_INET:
		isr = NETISR_IP;
		break;
	case AF_IPX:
		isr = NETISR_IPX;
		break;
	case AF_APPLETALK:
		isr = NETISR_ATALK2;
		break;
	default: 
		printf("if_simloop: can't handle af=%d\n", af);
		m_freem(m);
		return (EAFNOSUPPORT);
	}

	ifp->if_ipackets++;
	ifp->if_ibytes += m->m_pkthdr.len;
	netisr_queue(isr, m);
	return (0);
}

static void
smrtrequest(int cmd, struct rtentry *rt, struct rt_addrinfo *info)
{
	RT_LOCK_ASSERT(rt);
	rt->rt_rmx.rmx_mtu = rt->rt_ifp->if_mtu;
}

int
smioctl(struct ifnet *ifp, u_long cmd, caddr_t data)
{
	struct ifaddr *ifa;
	struct ifreq *ifr = (struct ifreq *)data;
	int error = 0, mask;

	switch (cmd) {
	case SIOCSIFADDR:
		ifp->if_flags |= IFF_UP;
		ifp->if_drv_flags |= IFF_DRV_RUNNING;
		ifa = (struct ifaddr *)data;
		ifa->ifa_rtrequest = smrtrequest;
		break;

	case SIOCADDMULTI:
	case SIOCDELMULTI:
		if (ifr == 0) {
			error = EAFNOSUPPORT;
			break;
		}
		switch (ifr->ifr_addr.sa_family) {
		case AF_INET:
			break;
		case AF_INET6:
			break;
		default:
			error = EAFNOSUPPORT;
			break;
		}
		break;

	case SIOCSIFMTU:
		ifp->if_mtu = ifr->ifr_mtu;
		break;
	
	case SIOCSIFFLAGS:
		break;
	
	case SIOCSIFCAP:
		mask = ifp->if_capenable ^ ifr->ifr_reqcap;
		if ((mask & IFCAP_RXCSUM) != 0)
			ifp->if_capenable ^= IFCAP_RXCSUM;
		if ((mask & IFCAP_TXCSUM) != 0)
			ifp->if_capenable ^= IFCAP_TXCSUM;
		if (ifp->if_capenable & IFCAP_TXCSUM)
			ifp->if_hwassist = SM_CSUM_FEATURES;	
		break;
	default:
		error = EINVAL;
	}

	return (error);
}
